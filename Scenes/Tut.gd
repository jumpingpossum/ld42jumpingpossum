extends TextureButton

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	visible = false

func _on_AnimatedSprite_animation_finished():
	visible = true
	
func _pressed():
	get_tree().change_scene("res://Scenes/Tutorial.tscn")
