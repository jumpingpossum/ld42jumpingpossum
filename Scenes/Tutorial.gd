extends Node2D

var stage = 1

func _ready():
	$Tutorial1.visible = true
	$Tutorial2.visible = false
	pass
	
func nextMenu(stage):
	
	if stage == 1:
		$Tutorial1.visible = true
		$Tutorial2.visible = false
	elif stage == 2:
		$Tutorial1.visible = false
		$Tutorial2.visible = true
	elif stage == 3:
		get_tree().change_scene("res://Scenes/MainMenu.tscn")
		pass
	else:
		stage = 3

func _on_TextureButton_pressed():
	stage = stage +1
	nextMenu(stage)
	pass # replace with function body
