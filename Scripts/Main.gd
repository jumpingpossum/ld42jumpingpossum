extends Node

var citizens = []

func _ready():
    Global.Main = self
    Ready()
    

func Ready():
    Global.CitizenLocations = []
    Global.Survivors = 0
    Global.Dead = 0
    Global.Turn = 0
    Global.MovementAmount = 0
    Global.WinningPositions = []
    Global.MovesPlanned = 0
    Global.MovesTaken = 0
    Global.HUD.UpdateMoveCounter()
    Global.HUD.UpdateHUD()
    Global.WinningPositions.append(Vector2(4,0))
    Global.WinningPositions.append(Vector2(5,0))
    Global.WinningPositions.append(Vector2(6,0))
    Global.WinningPositions.append(Vector2(6,17))
    Global.WinningPositions.append(Vector2(18,17))
    Global.WinningPositions.append(Vector2(19,17))
    Global.WinningPositions.append(Vector2(20,17))
    SpawnCitizens()

func SpawnCitizens():
    for i in Global.NumberOfCitizens:
        addCitizen()


func addCitizen():
    var citizenScene = load(Global.CitizenPath)
    var citizenInstance = citizenScene.instance()
    var nameString = "Citizen" + String(Global.CitizenNumber)
    citizenInstance.set_name(nameString)
    add_child(citizenInstance)
    citizens.append(citizenInstance)
    
