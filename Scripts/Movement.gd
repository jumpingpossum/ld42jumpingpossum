extends Node2D

var validWalkingTiles = [0,1,2,6,7]
var currentLocations = []
var selected = false
var citizen = null
var movers = []

func _ready():
    Global.Movement = self

func _unhandled_input(event):
    if Input.is_action_just_pressed("space"):
        Global.HUD.OnButtonEnd()        
    if selected && citizen != null && !Global.HUD.ending:
        var mousePosition = get_global_mouse_position()
        var tileLocation = Global.MainMap.world_to_map(mousePosition)
        if Input.is_action_just_pressed("MB1"):
            #if !currentLocations.has(tileLocation):
            if CheckClickValidity(tileLocation):
                SetPosition(tileLocation)
            for i in Global.Main.citizens.size():
                if Global.MainMap.world_to_map(Global.Main.citizens[i].location) == tileLocation:
                    citizen = null
                    selected = false
                    for i in Global.Main.citizens.size():
                        Global.Main.citizens[i].Deselect()
                    NewSelection(i)
        elif Input.is_action_just_pressed("MB2"):# && Global.NumberMap.get_cellv(tileLocation) == citizen.movementAmount:
            citizen.DeselectMovement(tileLocation)
    elif !selected && Input.is_action_just_pressed("MB1") && !Global.HUD.ending:
        var mousePosition = get_global_mouse_position()
        var tileLocation = Global.MainMap.world_to_map(mousePosition)
        for i in Global.Main.citizens.size():
            if Global.MainMap.world_to_map(Global.Main.citizens[i].location) == tileLocation:
                NewSelection(i)
                

func NewSelection(i):
    citizen = Global.Main.citizens[i]
    selected = true
    Global.Main.citizens[i].ToggleSelected()

func CheckClickValidity(tileLocation):
    var cell = Global.MainMap.get_cellv(tileLocation)
    for i in validWalkingTiles.size():
        if cell == validWalkingTiles[i]:
            return true
    return false

func SetPosition(tileLocation):
   citizen.AddMovement(tileLocation)
   if !movers.has(citizen):
    movers.append(citizen)
