extends Sprite

var location
var movementMap = {}
var movementAmount = 0
var Dead = false
var Won = false
signal walking

func _init():
    location = Global.MainMap.map_to_world(GetStartPosition())
    self.position = location

func AddMovement(theVector):
    if Global.MovementAmount < Global.WalkingDistance && CheckMoveValidity(theVector) && movementAmount < 5:
        Global.MovementAmount += 1
        movementAmount += 1
        movementMap[movementAmount] = Global.MainMap.map_to_world(theVector)
        Global.NumberMap.set_cellv(theVector, movementAmount-1)
        Global.HUD.UpdateMoveCounter()

func Walk():
    Deselect()
    var movesTaken = 0
    for i in range(movementAmount):
        var j = i+1
        if !Dead && !Won:
            yield(get_tree().create_timer(0.25), "timeout")
            location = movementMap[j]
            var direction = CheckMoveDirection(Global.MainMap.world_to_map(movementMap[j]))
            self.position = movementMap[j]
            emit_signal("walking")
            #Global.NumberMap.set_cellv(Global.MainMap.world_to_map(movementMap[j]), -1)
            ChangeSprite(direction)
            if Global.LavaMap.get_cellv(Global.LavaMap.world_to_map(location)) != -1 && !Won:
                Dead = true
                Global.Dead += 1
            if !Dead:
                for j in Global.WinningPositions.size():
                    if Global.WinningPositions[j] == Global.MainMap.world_to_map(self.position):
                        Global.Survivors += 1
                        Global.Score += 100
                        Won = true  
        Global.MovesTaken += 1
        Global.NumberMap.set_cellv(Global.MainMap.world_to_map(movementMap[j]), -1)    

    if Dead:
        Die(false)
    if Won && !Dead:        
        Victory()
    Reset()

func DeselectMovement(theVector):
    if movementAmount > 0:
        Global.NumberMap.set_cellv(Global.MainMap.world_to_map(movementMap[movementAmount]), -1)
        movementMap.erase(movementAmount)
        movementAmount -= 1
        Global.MovementAmount -= 1
        Global.HUD.UpdateMoveCounter()


func ToggleSelected():   
    if region_rect == Rect2(0,0,32,32):
        region_rect = Rect2(128,0,32,32)
    elif region_rect == Rect2(128,0,32,32):
        region_rect = Rect2(0,0,32,32)
    elif region_rect == Rect2(32,0,32,32): 
        region_rect = Rect2(160,0,32,32)
    elif region_rect == Rect2(160,0,32,32):
        region_rect = Rect2(32,0,32,32)
    elif region_rect == Rect2(64,0,32,32): 
        region_rect = Rect2(192,0,32,32)
    elif region_rect == Rect2(192,0,32,32):
        region_rect = Rect2(64,0,32,32)
    elif region_rect == Rect2(96,0,32,32): 
        region_rect = Rect2(224,0,32,32)
    elif region_rect == Rect2(224,0,32,32):
        region_rect = Rect2(96,0,32,32)

func Deselect():
    if region_rect == Rect2(128,0,32,32):
        region_rect = Rect2(0,0,32,32)
    elif region_rect == Rect2(160,0,32,32):
        region_rect = Rect2(32,0,32,32)
    elif region_rect == Rect2(192,0,32,32):
        region_rect = Rect2(64,0,32,32)
    elif region_rect == Rect2(224,0,32,32):
        region_rect = Rect2(96,0,32,32)

func ChangeSprite(direction):
    ToggleSelected()
    match direction:
        "UP":
            region_rect = Rect2(0,0,32,32)
        "RIGHT":
            region_rect = Rect2(32,0,32,32)
        "DOWN":
            region_rect = Rect2(64,0,32,32)
        "LEFT":
            region_rect = Rect2(96,0,32,32)


func CheckMoveDirection(theVector):
    var myPosition = Global.MainMap.world_to_map(position)
    if theVector == Vector2(myPosition.x + 1, myPosition.y):
        return "RIGHT"
    elif theVector == Vector2(myPosition.x - 1, myPosition.y):
        return "LEFT"
    elif theVector == Vector2(myPosition.x, myPosition.y + 1):
        return "DOWN"
    elif theVector == Vector2(myPosition.x, myPosition.y - 1):
        return "UP"
    else:
        return "UP"

func CheckMoveValidity(theVector):
    var myPosition
    for i in Global.Main.citizens.size():
        if Global.MainMap.world_to_map(Global.Main.citizens[i].position) == theVector:
            return false
    if movementAmount == 0:
        myPosition = Global.MainMap.world_to_map(position)
    else:
        myPosition = Global.MainMap.world_to_map(movementMap[movementAmount])
    if theVector == Vector2(myPosition.x + 1, myPosition.y):
        return true
    elif theVector == Vector2(myPosition.x - 1, myPosition.y):
        return true
    elif theVector == Vector2(myPosition.x, myPosition.y + 1):
        return true
    elif theVector == Vector2(myPosition.x, myPosition.y - 1):
        return true
    else:
        return false

func Reset():
    movementMap = {}
    movementAmount = 0

func Die(ThisIsaBool):#, movesTaken):
    if !Dead:
        Dead = true
#    var movesLeft = movementAmount - movesTaken
#    if movesLeft == 0:        
#        if Global.MovesTaken == Global.MovementAmount:
#            Global.MovesTaken = 0
#            Global.HUD.TurnEnded()
#            Global.MovementAmount = 0
#    for i in movesLeft:
#        Global.MovesTaken += 1
#        movesTaken += 1
#        Global.NumberMap.set_cellv(Global.MainMap.world_to_map(movementMap[movesTaken]), -1)
#    Reset()
    #region_rect = Rect2(64,0,32,32)
    Global.Main.citizens.erase(self)
    Global.LavaMap.set_cellv(Global.LavaMap.world_to_map(position), 5)
    Global.HUD.DeathScream()
    print("Died")
    if ThisIsaBool:
        print("Hit by lava")
        Global.HUD.DiedFromLava()
    queue_free()
    

func GetStartPosition():
    var testPosition = RandomPosition()
    for i in Global.Movement.validWalkingTiles.size():
        if Global.MainMap.get_cellv(testPosition) == Global.Movement.validWalkingTiles[i]:
            if Global.CitizenLocations.has(testPosition):
                return GetStartPosition()
            Global.CitizenLocations.append(testPosition)
            return testPosition
    return GetStartPosition()    

func Victory():#movesTaken):
    print("I Survived!")
    #var movesLeft = movementAmount - movesTaken
#    if movesLeft == 0:        
#        if Global.MovesTaken == Global.MovementAmount:
#            Global.MovesTaken = 0
#            Global.MovementAmount = 0
#    for i in movesLeft:
#        Global.MovesTaken += 1
#        movesTaken += 1
#        Global.NumberMap.set_cellv(Global.MainMap.world_to_map(movementMap[movesTaken]), -1)
#        if Global.MovesTaken == Global.MovementAmount:
#            Global.MovesTaken = 0
#            Global.MovementAmount = 0
#    Reset()
    Global.Main.citizens.erase(self)
    queue_free()



func RandomPosition():
        randomize()
        var x = randi()% 26+4
        var y = randi()% 10+3
        return Vector2(x,y)