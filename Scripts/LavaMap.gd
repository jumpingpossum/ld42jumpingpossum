extends TileMap

var lavaPositions = []
var lavaFailCounter = 0
signal death
signal spawningLava


func _ready():
    Global.LavaMap = self

func Death():
    $Wilhelm.death()
    
func Act():
    yield(get_tree().create_timer(0.5), "timeout")
    FindNewLavaPositions()  
    SpawnLava()

func SpawnLava():	
    var newLocation = RandomPosition()
    #emit_signal("spawningLava")
    Global.LavaAudio._on_TileMap_spawningLava()
    if lavaPositions.has(newLocation) && Global.MainMap.get_cellv(newLocation) == -1 && lavaFailCounter < 5:
        for i in Global.Main.citizens.size():
            if newLocation == Global.MainMap.worl_to_map(Global.Main.citizens[i]):
                lavaFailCounter += 1
                return SpawnLava()
        lavaFailCounter += 1
        return SpawnLava()
    var currentVector = newLocation
    if get_cellv(currentVector) == -1:
        set_cellv(currentVector, 0)
    lavaPositions.append(currentVector)

func FindNewLavaPositions():
    for i in lavaPositions.size():
        var xPositive = Vector2(lavaPositions[i].x + 1, lavaPositions[i].y)
        var xNegative = Vector2(lavaPositions[i].x - 1, lavaPositions[i].y)
        var yPositive = Vector2(lavaPositions[i].x, lavaPositions[i].y + 1)
        var yNegative = Vector2(lavaPositions[i].x, lavaPositions[i].y - 1)
        if Global.MainMap.get_cellv(xPositive) != -1 && get_cellv(xPositive) == -1:
            set_cellv(xPositive, 0)
            CheckForCitizens(xPositive)
            lavaPositions.append(xPositive)
        if Global.MainMap.get_cellv(xNegative) != -1 && get_cellv(xNegative) == -1:
            set_cellv(xNegative, 0)
            CheckForCitizens(xNegative)
            lavaPositions.append(xNegative)
        if Global.MainMap.get_cellv(yPositive) != -1 && get_cellv(yPositive) == -1:
            set_cellv(yPositive, 0)
            CheckForCitizens(yPositive)
            lavaPositions.append(yPositive)
        if Global.MainMap.get_cellv(yNegative) != -1 && get_cellv(yNegative) == -1:
            set_cellv(yNegative, 0)
            CheckForCitizens(yNegative)
            lavaPositions.append(yNegative)

func CheckForCitizens(checkPosition):
        for i in Global.Main.citizens.size():
            if checkPosition == world_to_map(Global.Main.citizens[i].location):
                Global.Dead += 1
                Global.Main.citizens[i].Dead = true
                Global.Main.citizens[i].Die( true)
                return


func RandomPosition():
    randomize()
    var x = randi()% 32
    var y = randi()% 18
    return Vector2(x,y)