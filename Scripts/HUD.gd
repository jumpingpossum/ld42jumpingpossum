extends CanvasLayer

var i = 0
var turnActive = false
var ending = false

func _ready():
    Global.HUD = self
    
func _process(delta):     
    if (Global.Dead + Global.Survivors) == Global.NumberOfCitizens:
        print("Calculating Score and ending the play")
        #TurnEnded()
        Global.RoundEndScore()
        Global.Survivors = 0
        Global.Dead = 0
        yield(get_tree().create_timer(.25), "timeout")
        get_tree().change_scene("res://Scenes/UpgradeMenu.tscn")

func UpdateMoveCounter():
    var currentMovesLeft = Global.WalkingDistance - Global.MovementAmount
    $CenterContainer/HBoxContainer/Label.text = String(currentMovesLeft)

func DeathScream():
    Global.LavaMap.Death()

func OnButtonEnd():
    
    if Global.MovementAmount > 0 && !turnActive:
        turnActive = true
        EndTurn()
        Global.Turn += 1
        var timer = .15
        for i in Global.MovementAmount:
            timer += 0.25
            
        if Global.LavaStart <= Global.Turn:
            yield(get_tree().create_timer(timer), "timeout")
            Global.LavaMap.Act()
        

func EndTurn():
    ending = true
    if Global.MovementAmount > 0:
        Global.Movement.selected = false
        for i in Global.Movement.movers.size():
            Global.Movement.movers[i].Walk()
            var timer = 0
            for j in Global.Movement.movers[i].movementAmount:
                timer += 0.25
            yield(get_tree().create_timer(timer), "timeout")
        TurnEnded()
            
func TurnEnded():
    Global.Movement.movers.clear()
    UpdateHUD()
    print("Turn " + String(Global.Turn) + "  ended")
    turnActive = false
    Global.MovementAmount = 0
    Global.MovesTaken = 0
    
    ending = false

func DiedFromLava():
    if (Global.Dead + Global.Survivors) == Global.NumberOfCitizens:
        Global.RoundEndScore()
        yield(get_tree().create_timer(.25), "timeout")
        get_tree().change_scene("res://Scenes/UpgradeMenu.tscn")
            
        
func UpdateHUD():
    yield(get_tree().create_timer(0.25), "timeout")
    UpdateMoveCounter()
    var score = Global.Score
    if score < 10:
        score = "000" + String(score)
    elif score < 100:
        score = "00" + String(score)
    elif score < 1000:
        score = "0" + String(score)
    var survivors = Global.Survivors
    if survivors < 10:
        survivors = "0" + String(survivors)
    var turn = Global.Turn
    if turn < 10:
        turn = "0" + String(turn)

    $Score/HBoxContainer/Score.text = String(score)
    $Score/HBoxContainer/Citizens.text = String(survivors)
    $Score/HBoxContainer/Count.text = String(turn)
    
