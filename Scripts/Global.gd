extends Node

# Stats
var Score = 0
var Survivors = 0
var Dead = 0
var Turn = 0

# Nodes
var Movement
var MainMap
var LavaMap
var Main
var HUD
var NumberMap
var LavaAudio

# Variables
var WalkingDistance = 1
var LavaStart = 1
var NumberOfCitizens = 1
var MovementAmount = 0
var WinningPositions = []
var MovesPlanned = 0
var MovesTaken = 0
var CitizenLocations = []

func RoundEndScore():
    if Survivors == NumberOfCitizens:
        Global.Score += Survivors * (20*Survivors) + 100
    for i in Survivors:
        randomize()
        Global.Score += randi()%120+30
    if Survivors == 0:
        Global.Score += 20
# Citizens
var CitizenNumber = 1
var CitizenPath = "res://Scenes/Citizen.tscn"

# Upgrade Costs
var citizenCost = 100
var delayCost = 150
var moveCost = 150
var freezeCost = 300

func CitizenCost():
    var cost = 0
    cost = citizenCost + ((citizenCost * (NumberOfCitizens*2)))
    return cost

func DelayCost():
    var cost = 0
    cost = delayCost + ((delayCost * (LavaStart*3)))
    return cost

func MoveCost():
    var cost = 0
    cost = moveCost + (moveCost * pow(WalkingDistance, 2))
    return cost