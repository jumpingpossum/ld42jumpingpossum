extends CanvasLayer

func ResumeGame():
    get_tree().change_scene("res://Scenes/Main.tscn")

func _ready():
    UpdateFields()

func UpdateFields():
    $Score.text = String(Global.Score)
    $CitizenCost.text = String(Global.CitizenCost())
    $RoundCost.text = String(Global.DelayCost())
    $MovementCost.text = String(Global.MoveCost())

func OneMoreCitizen():
    if Global.Score > Global.CitizenCost():
        Global.Score -= Global.CitizenCost()
        Global.NumberOfCitizens += 1
        UpdateFields()

func AdditionalRoundDelay():
    if Global.Score > Global.DelayCost():
        Global.Score -= Global.DelayCost()
        Global.LavaStart += 1
        UpdateFields()

func OneMoreMove():
    if Global.Score > Global.MoveCost():
        Global.Score -= Global.MoveCost()
        Global.WalkingDistance += 1
        UpdateFields()
        UpdateFields()

func OneMoreFreeze():
    if Global.Score > 750:
        print("You can afford this!")
    print("This function has not been added yet!")


